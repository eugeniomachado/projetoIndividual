LOGIN = new Object();
OPTIONS = new Object();
var optionsUrl = "/projetoIndividual/rest/optionsRest/";


$(document).ready( function() {
	//page loaders + links

	
	//OPTIONS.getSelectedTheme();
	
    $("#load_session").on("click", function() {
        $("#content").load("/projetoIndividual/resources/session/newsession.html");
        $("#costumerli").removeClass("active"); 
        $("#scheduleli").removeClass("active"); 
        $("#financesli").removeClass("active");
        $("#dashli").removeClass("active");
        $("#sessionli").addClass("active");
        
    });
    
    $("#load_costumer").on("click", function() {
        $("#content").load("/projetoIndividual/resources/customer/customer.html");
        $("#sessionli").removeClass("active");
        $("#scheduleli").removeClass("active");
        $("#financesli").removeClass("active");
        $("#dashli").removeClass("active");
        $("#costumerli").addClass("active");    
    });
    
    $("#load_schedule").on("click", function() {
    	$("#content").load("/projetoIndividual/resources/schedule/schedule.html");
        $("#sessionli").removeClass("active");
        $("#costumerli").removeClass("active"); 
        $("#financesli").removeClass("active");
        $("#dashli").removeClass("active");
        $("#scheduleli").addClass("active");  
    });
    
    $("#load_finances").on("click", function() {
    	$("#content").load("/projetoIndividual/resources/finances/finances.html");
        $("#sessionli").removeClass("active");
        $("#costumerli").removeClass("active"); 
        $("#scheduleli").removeClass("active");  
        $("#dashli").removeClass("active");
        $("#financesli").addClass("active");  
    });
    
    $("#load_dashboard").on("click", function() {
        $("#content").load("/projetoIndividual/resources/dashboard/dashboard.html");
        $("#costumerli").removeClass("active"); 
        $("#scheduleli").removeClass("active"); 
        $("#financesli").removeClass("active");
        $("#dashli").addClass("active");
        
    });
    
    $("#load_options").on("click", function() {
        $("#content").load("/projetoIndividual/resources/index/options.html");
        $("#costumerli").removeClass("active"); 
        $("#scheduleli").removeClass("active"); 
        $("#financesli").removeClass("active");
        $("#dashli").removeClass("active");
        
    });
    
    
    
 
});


OPTIONS.changeColour = function(){
	var colour = $('#edit-theme').val();
	$('#sidecolour').attr("data-color", colour);
	
	var json = {
			id : '1',
			colour : colour
	}
	 $.ajax({
	        type: "POST",
	        url: optionsUrl + colour,
	        data: JSON.stringify(json),
	        dataType: "json",
	        success: function() {
	      
	        },
	        error: function(msg) {


	        }
	    });
},

OPTIONS.changeImage = function(){
	var image = $('#edit-image').val();
	$('#sidecolour').attr("data-image", image);
	$(".sidebar-background").css("background-image", image);
	
},

OPTIONS.getSelectedTheme = function(){
	
},

LOGIN.logout = function(){
	$.ajax({
		type : "POST",
		url : '/projetoIndividual/Logout',
		success : function() {
			//window.location = "/projetoIndividual/";

		},
		error : function() {
		
		}
	});
	
}

