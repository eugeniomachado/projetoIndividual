Session = new Object();

$.notifyDefaults({
    placement: {
        from: "top",
        align: "center"
    },
});


var table = $('#example').DataTable();

$(document).ready(function(id) {	
	var id_customer = $("#info-customer-id").val();
	//Edit Modal
    $('#editSession').on('show.bs.modal', function(event) {
    }) 
    if(id_customer != null && id_customer != undefined && id_customer != null){
		$.ajax({
		    type: "GET",
		    url: "/projetoIndividual/rest/sessionRest/listSessions/" + id_customer,
		    dataType: 'json',
			contentType: "application/json;charset=UTF-8",
	        beforeSend: function() {
	            $('#loader').show();
	        },
	        complete: function() {
	            $('#loader').hide();
	            $('#loader').addClass('hideLoader');
	        },
		    success: function(sessionlist) {
		        Session.listSessions(sessionlist);
		    },
		    error: function(err) {
		        $.notify({
		            message: 'Houve um erro ao carregar a lista de clientes!'
		        }, {
		            type: 'danger'
		        });
		    }
		})
    }
});


Session.saveSession = function(){
    var presentCheck = $('#present').find(":selected").text();
    var present = true;
    var register = CKEDITOR.instances.editor1.getData();
    
    if (presentCheck == "Sim") {
        present = true
    } else {
        present = false
    }
    
    var session = {};
    session.date = $("#date").val();
    session.present = present;
    session.session_register = register;
    session.customer = {"id_customer": $("#info-customer-id").val()};
   
    $.ajax({
        type: "POST",
        url: "/projetoIndividual/rest/sessionRest/newSession/",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(session),
        dataType: "json",
        success: function() {
        	var id_customer = session.customer.id_customer;
  
            $('#newSession').modal('hide');
            reloader(id_customer);
            
            $.notify({
                message: 'Sessão cadastrado com sucesso!'
            }, {
                type: 'success'
            });
        },
        error: function(err) {
            $.notify({
                message: 'Erro ao cadastrar uma nova sessão'
            }, {
                type: 'danger'
            });
        }
    })
};

reloader = function(id_customer){
	if(id_customer != null && id_customer != undefined && id_customer != null){
		$.ajax({
		    type: "GET",
		    url: "/projetoIndividual/rest/sessionRest/listSessions/" + id_customer,
		    dataType: 'json',
			contentType: "application/json;charset=UTF-8",
			 beforeSend: function() {
		            $('#loader').show();
		        },
		        complete: function() {
		            $('#loader').hide();
		            $('#loader').addClass('hideLoader');
		        },
		    success: function(sessionlist) {
		    	Session.listSessions(sessionlist);
		    },
		})
	}
},
Session.listSessions = function(sessionlist) {
    var arr = [];
    sessionlist.forEach(function(obj) {	
    	var truncate = jQuery.trim(obj.session_register).substring(0, 100)
        .trim(this) + "...";
    	
        //var date = new Date(obj.formated_date);
        //var sessiondate = date.toISOString().slice(0, 10);
        
        var linkedit = '<a data-toggle="modal" href="#sessionEdit" class="editidsess" data-id="' + obj.id_session + '" ><i class="pe-7s-bookmarks"></i></a>'
        var linkinfo = '<a data-toggle="modal" href="#sessionInfo" class="getid" data-id="' + obj.id_session +
            '"onclick="Session.details(' + obj.id_session + ')" ><i class="pe-7s-plus"></i></a>'
        var present = "";    
        if(obj.present){
        	present = "Presente"
        }else{
            present = "Faltou";
        }
        
        arr.push([obj.formated_date, present, truncate, linkedit, linkinfo]);
    })

  $('#sessionsDatatable').DataTable({
        data: arr,
        bDestroy: true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
        }
    })
    
},

$(document).on('click', '.editidsess', function() {
    var id_session = $(this).attr('data-id');
    $("#session-id").attr("value", id_session);
    Session.findById(id_session);  
});

Session.findById = function(id_session){
	 $.ajax({
         type: "GET",
         url: "/projetoIndividual/rest/sessionRest/searchById/" + id_session,
         contentType: "application/json;charset=utf-8",
         dataType: "json",
         success: function(session) {
             var date = new Date(session.date);
             var sessiondate = date.toISOString().slice(0, 10);
             
 			if (session.present){
				$("#present-edit").val("Sim");
			}else{
				$("#present-edit").val("Não");
			}
             
        	$('#date-edit').val(sessiondate);
        	CKEDITOR.instances.editor2.setData(session.session_register);
        	      	
         },
         error: function(err) {
             $.notify({
                 message: 'Houve um erro ao tentar buscar a sessão, tente novamente!'
             }, {
                 type: 'danger'
             });
         }
     })
 },


Session.edit = function(session) {
	 var presentCheck = $('#present-edit').find(":selected").text();
	    var present = true;
	    var register = CKEDITOR.instances.editor2.getData();
	    
	    if (presentCheck == "Sim") {
	        present = true
	    } else {
	        present = false
	    }
	    debugger
	    var session = {};
	    session.id_session = $("#session-id").val();
	    session.date = $("#date-edit").val();
	    session.present = present;
	    session.session_register = register;
	    session.customer = {"id_customer": $("#info-customer-id").val()};
	    $.ajax({
	        type: "POST",
	        url: "/projetoIndividual/rest/sessionRest/editSession/",
	        contentType: "application/json; charset=utf-8",
	        data: JSON.stringify(session),
	        dataType: "json",
	        success: function(session) {
	        	reloader(id_customer);
	        	$('#sessionEdit').modal('hide');

		        $.notify({
		            message: 'Sessão editada com sucesso'
		        }, {
		            type: 'success'
		        });
	        },
	        error: function(err) {
	            $.notify({
	                message: 'Erro ao cadastrar uma nova sessão'
	            }, {
	                type: 'danger'
	            });
	        }
	    })	
},

Session.details = function (id_session){
  	CKEDITOR.instances.editor3.resize( '100%', '400', true );
	$.ajax({
        type: "GET",
        url: "/projetoIndividual/rest/sessionRest/searchById/" + id_session,
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function(session) {
            var date = new Date(session.date);
            var sessiondate = date.toISOString().slice(0, 10);
            $("#session-id").attr("value", id_session);
			if (session.present){
				$("#present-edit").val("Sim");
			}else{
				$("#present-edit").val("Não");
			}
            
       	$('#date-session-info').val(sessiondate);
       	CKEDITOR.instances.editor3.setData(session.session_register);
       	      	
        },
        error: function(err) {
            $.notify({
                message: 'Houve um erro ao tentar buscar a sessão, tente novamente!'
            }, {
                type: 'danger'
            });
        }
	})
},

Session.delete = function (){
	var id_session = $('#session-id').val();
	console.log(id_session);
	$.ajax({
        type: "GET",
        url: "/projetoIndividual/rest/sessionRest/deleteSession/" + id_session,
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function() {
        	id_customer = $("#info-customer-id").val();
        	reloader(id_customer);
        	$('#sessionInfo').hide();
            $.notify({
                message: 'Sessão deletada com sucesso!'
            }, {
                type: 'success'
            });
        },
        error: function(err) {
            $.notify({
                message: 'Houve um erro ao deletar a sessão, tente novamente!'
            }, {
                type: 'danger'
            });
        }
	})
}
	
