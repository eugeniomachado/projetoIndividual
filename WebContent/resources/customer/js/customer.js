Customer = new Object();
var that = this;
//notifications
$.notifyDefaults({
    placement: {
        from: "top",
        align: "center"
    },
});

var idglobal = null;

$(document).ready(function() {
    Customer.create = function() {

        var name = $("#customer-name").val();
        var sex = $("#customer-sex").val();
        var birth_date = $("#birth").val();
        var phone = $("#customer-phone").val();
        var service = $("#customer-service").val();

        var error = false;

        if (name == "") {
            $("#namediv").addClass("has-error");
            $("#customer-name").focus();
            error = true;
        }


        if (sex == null) {
            $("#sexdiv").addClass("has-error");
            $("#customer-sex").focus();
            error = true;
        }

        if (birth_date == null) {
            $("#birthdiv").addClass("has-error");
            $("#birth").focus();
            error = true;
        }

        if (phone == "") {
            $("#phonediv").addClass("has-error");
            $("#customer-phone").focus();
            error = true;
        }

        if (service == "") {
            $("#servicediv").addClass("has-error");
            $("#customer-service").focus();
            error = true;
        }

        if (error) {
            $.notify({
                message: 'Favor preencher todos os campos obrigatórios!'
            }, {
                type: 'danger'
            });
            return false;
        }
        
        var check = "";
        var minorCheck = $("#edit-customer-fathers-name").val();

        var json = {
            name: $("#customer-name").val(),
            sex: $("#customer-sex").val(),
            birth_date: $("#birth").val(),
            email: $("#customer-mail").val(),
            phone: $("#customer-phone").val(),
            indicated_by: $("#customer-indicate").val(),
            service_type: $("#customer-service").val(),
            level_schooling: $("#customer-level-schooling").val(),
            grade_period: $("#customer-period").val(),
            institution: $("#customer-institution").val(),
            minor: $("#switch").bootstrapSwitch('state'),
            status: "Ativo"
        }

        var jsonMinor = {
            name: $("#customer-name").val(),
            sex: $("#customer-sex").val(),
            birth_date: $("#birth").val(),
            email: $("#customer-mail").val(),
            phone: $("#customer-phone").val(),
            indicated_by: $("#customer-indicate").val(),
            service_type: $("#customer-service").val(),
            level_schooling: $("#customer-level-schooling").val(),
            grade_period: $("#customer-period").val(),
            institution: $("#customer-institution").val(),
            minor: $("#switch").bootstrapSwitch('state'),
            status: "Ativo",
            father_name: $("#customer-fathers-name").val(),
            father_phone: $("#customer-father-phone").val(),
            mother_name: $("#customer-mothers-name").val(),
            mother_phone: $("#customer-mother-phone").val(),
            marital_status: $("#customer-parents-marital").val(),
        }
        
        if (minorCheck == "") {
            check = json;
        } else {
            check = jsonMinor;
        }
        
        $.ajax({
            url: "/projetoIndividual/rest/customerRest/newCustomer",
            type: "POST",
            data: JSON.stringify(check),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function(data) {
                $('#newCustomer').modal('hide');
                $("#content").load("/projetoIndividual/resources/customer/customer.html");
                $.notify({
                    message: 'Cliente cadastrado com sucesso!'
                }, {
                    type: 'success'
                });
                Customer.list();
            },
            error: function(err) {
                $.notify({
                    message: 'Problemas ao cadastrar um novo cliente!'
                }, {
                    type: 'danger'
                });
            }
        })

    }

    $(document).ready(function() {
        $('#newCustomer').on('show.bs.modal', function(event) {

        })
        
        $('#editCustomer').on('show.bs.modal', function(event) {

        })
    });

    $(document).click(function() {
        $("[name='my-checkbox']").bootstrapSwitch();
        $.fn.bootstrapSwitch.defaults.size = 'normal';
        $.fn.bootstrapSwitch.defaults.state = 'false';
    });

    $('#switch').on('switchChange.bootstrapSwitch', function(event, state) {
        if (event.target.checked) {
            $("#hidden-div").removeClass("hide");
        } else {
            $("#hidden-div").addClass("hide");
        }
    });

    Customer.search = function() {
        var name = "Joao";
        $.ajax({
            type: "POST",
            url: "/projetoIndividual/rest/customerRest/searchByName/" + name,
            success: function(listDates) {
                SESSIONCRUD.session.showSessions(listDates);
            },
            error: function(err) {
                $.notify({
                    message: 'Houve um erro ao tentar buscar um cliente, tente novamente!'
                }, {
                    type: 'danger'
                });
            }
        })
    }
});

//Rest request for all customers
$(document).ready(function() {
    $.ajax({
        type: "GET",
        url: "/projetoIndividual/rest/customerRest/listCustomers/",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function() {
            $('#loader').show();
        },
        complete: function() {
            $('#loader').hide();
            $('#loader').addClass('hideLoader');
        },
        success: function(customerList) {
            Customer.list(customerList);
            Customer.graphics();
            Customer.graphicsService();
        },
        error: function(err) {
            $.notify({
                message: 'Houve um erro ao carregar a lista de clientes!'
            }, {
                type: 'danger'
            });
        }
    })
});

Customer.list = function(customerList) {
    var arr = [];
    customerList.forEach(function(obj) {
        var linkedit = '<a data-toggle="modal" href="#editCustomer" class="editid" data-id="' + obj.id_customer + '"><i class="pe-7s-bookmarks"></i></a>'
        var linkinfo = '<a href="#" class="getid" data-id="' + obj.id_customer +
            '"onclick="Customer.info(' + obj.id_customer + ')" ><i class="pe-7s-plus"></i></a>'
        var statuscheck = obj.status;
        arr.push([obj.name, obj.service_type, obj.status, linkedit, linkinfo]);
    })

    $('#customerDatatable').DataTable({
        data: arr,
        bDestroy: true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
        }
    })
};

$(document).on('click', '.editid', function() {
    var id_customer = $(this).attr('data-id');
    Customer.searchById(id_customer);
    
});

Customer.graphics = function(){
	
	var arr = [];
	$.ajax({
	    type: "GET",
	    url: "/projetoIndividual/rest/customerRest/customerStatusGraphic",
	    contentType: "application/json; charset=utf-8",
	    dataType: "json",
	    success: function(data) {

	    	for (var i = 0; i < data.length; i ++){
	    		for(var x = 0; x < data[i].length; x++){
	    			arr.push(data[i][x]);
	    		}
	    	}
	    	var active = arr[1];
	    	var inactive = arr[3];

	    	
	    	var ctx = document.getElementById("graphic").getContext('2d');
	    	var myChart = new Chart(ctx, {
	    	  type: 'pie',
	    	  data: {
	    	    labels: ["Ativos", "Inativos"],
	    	    datasets: [{
	    	      backgroundColor: [
	    	        "#2ecc71",
	    	        "#3498db",
	    	      ],
	    	      data: [active, inactive]
	    	    }]
	    	 
	    	  }
	    	})

	    },
	    error: function(msg) {
	    }

	});
}

Customer.graphicsService = function(){
	
	var arr = [];
	setTimeout(function(){
	$.ajax({
	    type: "GET",
	    url: "/projetoIndividual/rest/customerRest/customerServiceGraphic",
	    contentType: "application/json; charset=utf-8",
	    dataType: "json",
	    success: function(data) {
	    	for (var i = 0; i < data.length; i ++){
	    		for(var x = 0; x < data[i].length; x++){
	    			arr.push(data[i][x]);
	    			
	    		}
	    	}
	    	console.log(arr);
	    	var orientation = arr[1];
	    	var psychotherapy = arr[3];
	    	var grouppsychotherapy = arr[5];
	    	var homecare = arr[7];
	    	var parentaltrain = arr[9];

	    	var ctx = document.getElementById("graphicservice").getContext('2d');
	    	var myChart = new Chart(ctx, {
	    	  type: 'pie',
	    	  data: {
	    	    labels: ["Orientação Educacional", "Psicoterapia", "Grupos", "Homecare", "Treino Parental"],
	    	    datasets: [{
	    	      backgroundColor: [
	    	        "#DC143C",
	    	        "#00008B",
	    	        "#E9967A",
	    	        "#228B22",
	    	        "#87CEFA"
	    	      ],
	    	      data: [orientation, psychotherapy, grouppsychotherapy, homecare, parentaltrain]
	    	    }]
	    	 
	    	  }
	    	})

	    },
	    error: function(msg) {
	    }

	});
	 }, 500);
}

Customer.searchById = function(id_customer) {
        $.ajax({
            type: "GET",
            url: "/projetoIndividual/rest/customerRest/searchById/" + id_customer,
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function(customer) {
                if (customer.minor) {
                    $("#edit-customer-fathers-name").attr("value", customer.father_name);
                    $("#edit-customer-father-phone").attr("value", customer.father_phone);
                    $("#edit-customer-mothers-name").attr("value", customer.mother_name);
                    $("#edit-customer-mothers-phone").attr("value", customer.mother_phone);
                    $("#edit-customer-parents-marital").attr("value", customer.marital_status);
                    $("#hiddenedit").show();
                } else {
                    $("#hiddenedit").hide();
                }

                if (customer.sex == "m") {
                    $("#edit-customer-sex").val('Masculino');
                }
                if (customer.sex == "f") {
                    $("#edit-customer-sex").val('Feminino');
                }
                if (customer.sex == "o") {
                    $("#edit-customer-sex").val('Outros');
                }
                     
                var date = new Date(customer.birth_date);
                var currentDate = date.toISOString().slice(0, 10);
                $("#edit-customer-id").attr("value", customer.id_customer);
                $("#edit-customer-name").val(customer.name);
                $("#edit-customer-sex").attr("selected", customer.sex);
                $("#edit-customer-birth").attr("value", currentDate);
                $("#edit-customer-mail").attr("value", customer.email);
                $("#edit-customer-phone").attr("value", customer.phone);
                $("#edit-customer-indicate").attr("value", customer.indicated_by);
                $("#edit-customer-service").val(customer.service_type);
                $("#edit-customer-level-schooling").attr("value", customer.level_schooling);
                $("#edit-customer-period").attr("value", customer.grade_period);
                $("#edit-customer-institution").attr("value", customer.institution);
                $("#status-edit").attr("value", customer.status);
                            
            },
            error: function(err) {
                $.notify({
                    message: 'Houve um erro ao tentar buscar um cliente, tente novamente!'
                }, {
                    type: 'danger'
                });
            }
        })
    },

    Customer.edit = function() {
        var storesex = $("#edit-customer-sex").val();
        if (storesex == "Masculino") {
            storesex = "m";
        } else if (storesex == "Feminino") {
            storesex = "f";
        } else {
            storesex = "o";
        }

        var check = "";
        var minorCheck = $("#edit-customer-fathers-name").val();

        var json = {
            id_customer: $("#edit-customer-id").val(),
            name: $("#edit-customer-name").val(),
            sex: storesex,
            birth_date: $("#edit-customer-birth").val(),
            email: $("#edit-customer-mail").val(),
            phone: $("#edit-customer-phone").val(),
            indicated_by: $("#edit-customer-indicate").val(),
            service_type: $("#edit-customer-service").val(),
            level_schooling: $("#edit-customer-level-scholing").val(),
            grade_period: $("#edit-customer-period").val(),
            institution: $("#edit-customer-institution").val(),
            status:$("#status-edit option:selected").val(),
            minor: false,
        }

        var jsonMinor = {
            id_customer: $("#edit-customer-id").val(),
            name: $("#edit-customer-name").val(),
            sex: storesex,
            birth_date: $("#edit-customer-birth").val(),
            email: $("#edit-customer-mail").val(),
            phone: $("#edit-customer-phone").val(),
            indicated_by: $("#edit-customer-indicate").val(),
            service_type: $("#edit-customer-service").val(),
            level_schooling: $("#edit-customer-level-scholing").val(),
            grade_period: $("#edit-customer-period").val(),
            institution: $("#edit-customer-institution").val(),
            status: $("#status-edit option:selected").val(),
            minor: true,
            father_name: $("#edit-customer-fathers-name").val(),
            father_phone: $("#edit-customer-father-phone").val(),
            mother_name: $("#edit-customer-mothers-name").val(),
            mother_phone: $("#edit-customer-mothers-phone").val(),
            marital_status: $("#edit-customer-parents-marital").val()
        }

        if (minorCheck == "") {
            check = json;

        } else {
            check = jsonMinor;

        }

        $.ajax({
            type: "POST",
            url: "/projetoIndividual/rest/customerRest/editCustomer/",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(check),
            dataType: "json",
            success: function(data) {
                $('#editCustomer').modal('hide');
                $("#content").load("/projetoIndividual/resources/customer/customer.html");
                $.notify({
                    message: 'Cliente editado com sucesso!'
                }, {
                    type: 'success'
                });
                Customer.list();
            },
            error: function(err) {
                $.notify({
                    message: 'Erro ao editar o cliente, tente novamente!'
                }, {
                    type: 'danger'
                });
            }
        });
    }

Customer.info = function(id_customer) {
    $("#content").load(
        "/projetoIndividual/resources/customer/customerinfo.html", id_customer);
    $.ajax({
        type: "GET",
        url: "/projetoIndividual/rest/customerRest/searchById/" + id_customer,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(customer) {
        	$("#info-customer-id").attr("value", customer.id_customer);
            if (customer.minor) {
                $("#edit-customer-fathers-name").attr("value", customer.father_name);
                $("#hidden-div-info").show();
            } else {
                $("#edit-customer-fathers-name").attr("value", customer.father_name);
                $("#hidden-div-info").hide();
            }

            var checksex = customer.sex;
            if (checksex == "m") {
                $("#customer-sex-info").val('m');
            }
            if (checksex == "f") {
                $("#customer-sex-info").val('f');
            }
            if (checksex == "o") {
                $("#customer-sex-info-sex").val('o');
            }
            debugger
            var date = new Date(customer.birth_date);
            var birthDate = date.toISOString().slice(0, 10);
            $("#customer-name-info").val(customer.name);
            $("#birth-info").val(birthDate);
            $("#customer-mail-info").val(customer.email);
            $("#customer-phone-info").val(customer.phone);
            $("#customer-indicate-info").val(customer.indicated_by);
            $("#customer-level-schooling-info").val(customer.level_schooling);
            $("#customer-period-info").val(customer.grade_period);
            $("#customer-institution").val(customer.institution);
            $("#birth-info").val(birthDate);
            $("#customer-fathers-name-info").val(customer.father_name);
            $("#customer-father-phone-info").val(customer.father_phone);
            $("#customer-mothers-name").val(customer.mother_name);
            $("#customer-mother-phone-info").val(customer.mother_phone);
            $("#customer-parents-marital-info").val(customer.marital_status);
                     
        },
        error: function(err) {
            $.notify({
                message: 'Erro ao retornar as informações do cliente'
            }, {
                type: 'danger'
            });
        }
    })
};

back = function(){
    $("#content").load("/projetoIndividual/resources/customer/customer.html");
}
