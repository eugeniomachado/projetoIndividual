package br.com.projetoIndividual.loginServlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;  

public class LogoutServlet extends HttpServlet {  

	private static final long serialVersionUID = 1L;

    public LogoutServlet() {
        // TODO Auto-generated constructor stub
    }
    
		private void process(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
	
	            request.getSession().invalidate();
	            response.sendRedirect(request.getContextPath() + "/");
		
		}
	    
		/**
		 * @param context 
		 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
		 */

		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {

					process(request, response);
			
			
		}
		
		protected void doGet(HttpServletRequest request, HttpServletResponse response)  
                                throws ServletException, IOException {  
			process(request, response);

            
    }  
}  