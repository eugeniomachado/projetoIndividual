package br.com.projetoIndividual.loginServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.projetoIndividual.db.connection.Connec;
import br.com.projetoIndividual.hash.Password.CannotPerformOperationException;
import br.com.projetoIndividual.jdbc.JDBCLoginDAO;
import br.com.projetoIndividual.objects.Admin;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public LoginServlet() {
        // TODO Auto-generated constructor stub
    }
    
	private void process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException, CannotPerformOperationException {
		try {
		PrintWriter out = response.getWriter();
		Admin admin = new Admin();
		admin.setLogin(request.getParameter("user"));
		admin.setPassword(request.getParameter("password"));	
		
		Connec conec = new Connec();
		Connection connection = conec.openConnection();
		JDBCLoginDAO login = new JDBCLoginDAO(connection);
		String password = login.checkLogin(admin);
		boolean retorno = login.validateLogin(admin, password);
		conec.closeConnection();
		
		if (retorno) {
			HttpSession sessao = request.getSession();
			sessao.setAttribute("login", request.getParameter("user"));
			response.setStatus(200);
		} else {
			response.setStatus(403);
		}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
    
	/**
	 * @param context 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			try {
				process(request, response);
			} catch (CannotPerformOperationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
