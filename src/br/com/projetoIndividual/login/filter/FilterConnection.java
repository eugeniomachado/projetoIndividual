package br.com.projetoIndividual.login.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class FiltroConexao
 */
public class FilterConnection implements Filter {

    /**
     * Default constructor. 
     */
    public FilterConnection() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String context = request.getServletContext().getContextPath();
		try {
			HttpSession session = ((HttpServletRequest) request).getSession();
			String user = null;
			if (session != null) {
				user = (String) session.getAttribute("login");
			}
			if (user == null) {
				session.setAttribute("msg", "Voc� n�o esta logado no sistema!");
				((HttpServletResponse) response).sendRedirect(context + "/login.html");
			} else {
				chain.doFilter(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
