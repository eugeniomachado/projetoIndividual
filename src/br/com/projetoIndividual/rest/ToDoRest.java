package br.com.projetoIndividual.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;

import br.com.projetoIndividual.business.ToDoBusiness;
import br.com.projetoIndividual.pojo.ToDo;

@Path("toDoRest")
public class ToDoRest extends UtilRest {
	public ToDoRest(){
		
	}
	
	ToDoBusiness toDoBusiness = new ToDoBusiness();
	
	@POST
	@Path("/new")				
	@Consumes("application/*")
	public Response newToDo(String json) {
		try {
			ToDo toDo = new ObjectMapper().readValue(json, ToDo.class);
			toDoBusiness.create(toDo);
			return this.buildResponse("cadastrado com sucesso");
		} catch (IOException e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}
	}
	
	@GET
	@Path("/list")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response list() {
		try {
			List<ToDo> toDoList = new ArrayList<ToDo>();
			toDoList = toDoBusiness.list();
			return this.buildResponse(toDoList);
		} catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}
	}
	
	@DELETE
	@Path("del/{id}")
	@Consumes("application/*")
	public Response delete(@PathParam("id") int id){
		try{
			toDoBusiness.delete(id);
		return this.buildResponse("Deletado com sucesso");
		}catch(Exception e){
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
			
		}
		
	}
	
}
