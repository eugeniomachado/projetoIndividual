package br.com.projetoIndividual.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;

import br.com.projetoIndividual.business.CustomerBusiness;
import br.com.projetoIndividual.business.OptionsBusiness;
import br.com.projetoIndividual.pojo.Customer;
import br.com.projetoIndividual.pojo.CustomerVO;
import br.com.projetoIndividual.pojo.Options;

@Path("optionsRest")
public class OptionsRest extends UtilRest{
	public OptionsRest() {
	}
	
	OptionsBusiness optionBusiness = new OptionsBusiness();
	
	@POST
	@Path("/saveColour")
	@Consumes("application/*")
	public Response saveColour (@PathParam("colour") String colour) {
		try {
			Options options = new ObjectMapper().readValue(colour, Options.class);
			optionBusiness.saveColour(options);
			return this.buildResponse("");
		} catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}
	}
	
	
	@GET
	@Path("/get")
	@Consumes("application/*")
	public Response getConfig () {
		try {

			optionBusiness.getConfig();
			return this.buildResponse("Cliente cadastrado com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}
	}
	
}	

