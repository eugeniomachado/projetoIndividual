package br.com.projetoIndividual.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;

import br.com.projetoIndividual.business.ScheduleBusiness;
import br.com.projetoIndividual.pojo.Schedule;

@Path("scheduleRest")
public class ScheduleRest extends UtilRest{
	public ScheduleRest() {
	}
	
	ScheduleBusiness scheBusiness = new ScheduleBusiness();
	
	@POST
	@Path("/newEvent")
	@Consumes("application/*")
	public Response newEvent (String obj) {
		try {
			Schedule schedule = new ObjectMapper().readValue(obj, Schedule.class);
			scheBusiness.newEvent(schedule);
			return this.buildResponse("Evento cadastrado com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}
	}
		
	@GET
	@Path("/listEvents")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response listEvents(){
		try{
			List<Schedule> eventList = new ArrayList<Schedule>();
			System.out.println(eventList);
			eventList = scheBusiness.list();
			return this.buildResponse(eventList);
		}catch (Exception e){
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}		
	}
	
	@DELETE
	@Path("/delete/{id}")
	@Consumes("application/*")
	public Response delete(@PathParam("id") int id){
		try{
		scheBusiness.delete(id);
		return this.buildResponse("Deletado com sucesso");
		}catch(Exception e){
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
			
		}
		
	}
	
	@PUT
	@Path("/update")
	@Consumes("application/*")
	public Response editEvent (String data) {
		try {
			Schedule schedule = new ObjectMapper().readValue(data, Schedule.class);
			scheBusiness.editEvent(schedule);
			return this.buildResponse("Atualizado com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}
	}
	

}
	



