package br.com.projetoIndividual.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;

import br.com.projetoIndividual.business.CustomerBusiness;
import br.com.projetoIndividual.pojo.Customer;
import br.com.projetoIndividual.pojo.CustomerVO;

@Path("customerRest")
public class CustomerRest extends UtilRest{
	public CustomerRest() {
	}
	CustomerBusiness custBusiness = new CustomerBusiness();
	
	@POST
	@Path("/newCustomer")
	@Consumes("application/*")
	public Response newCustomer (String json) {
		try {
			Customer customer = new ObjectMapper().readValue(json, Customer.class);
			custBusiness.createCustomer(customer);
			return this.buildResponse("Cliente cadastrado com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}
	}
	
	@GET
	@Path("/listCustomers")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response listCustomers(){
		try{
			List<Customer> customerList = new ArrayList<Customer>();
			customerList = custBusiness.list();
			return this.buildResponse(customerList);
		}catch (Exception e){
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}		
	}
	
	
	@GET
	@Path("/searchById/{id_customer}")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response searchById(@PathParam("id_customer") int id){
		try{
			Customer customer = custBusiness.find(id);
			return this.buildResponse(customer);
	
		}catch (Exception e){
			System.out.println("errou");
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}		
	}
	
	
	@POST
	@Path("/editCustomer")
	@Consumes("application/*")
	public Response editCustomer (String json) {
		try {
			Customer customer = new ObjectMapper().readValue(json, Customer.class);
			custBusiness.editCustomer(customer);
			return this.buildResponse("Cliente editado com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}
	}
	
	
	@GET
	@Path("/customerStatusGraphic")
	@Consumes("application/*")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})	
	public Response customerStatusGraphic(){
		try{
			List<Customer> result = custBusiness.customerStatusGraphic();
			return this.buildResponse(result);
		}catch (Exception e){
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}		
	}
	
	@GET
	@Path("/customerServiceGraphic")
	@Consumes("application/*")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})	
	public Response customerServiceGraphic(){
		try{
			List<Customer> result = custBusiness.customerServiceGraphic();
			return this.buildResponse(result);
		}catch (Exception e){
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}		
	}
	

	@GET
	@Path("/getMontlyAnniversary")
	@Consumes("application/*")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})	
	public Response getMontlyAnniversary(){
		try{
			List<CustomerVO> result = custBusiness.getMontlyAnniversary();
			return this.buildResponse(result);
		}catch (Exception e){
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}		
	}

	
	
}	

