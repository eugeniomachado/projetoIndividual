package br.com.projetoIndividual.rest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;

import br.com.projetoIndividual.business.FinancesBusiness;
import br.com.projetoIndividual.pojo.Finances;
import br.com.projetoIndividual.pojo.FinancesVO;

@Path("financesRest")
public class FinancesRest extends UtilRest {
	public FinancesRest() {
		System.out.println("*** Criou o REST ***");
	}
	
	FinancesBusiness financesBusiness = new FinancesBusiness();	
	
	@POST
	@Path("/new")
	@Consumes("application/*")
	public Response newRegister (String obj) {
		try {
			String obj2 = financesBusiness.formatMoni(obj);
		
			Finances finances = new ObjectMapper().readValue(obj2, Finances.class);
			financesBusiness.newFinance(finances);
			return this.buildResponse("Finan�a cadastrado com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}
	}
	
	@GET
	@Path("/list")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response list(){
		try{
			List<Finances> financeList = new ArrayList<Finances>();
			financeList = financesBusiness.list();
			return this.buildResponse(financeList);
		}catch (Exception e){
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}		
	}
	
	@GET
	@Path("/find/{id_finance}")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response findById(@PathParam("id_finance") int id_finance){
		try{
			Finances finance =  financesBusiness.find(id_finance);
			return this.buildResponse(finance);
		}catch (Exception e){
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}		
	}

	
	@POST
	@Path("/edit")
	@Consumes("application/*")
	public Response editCustomer (String json) {
		try {
			String obj2 = financesBusiness.formatMoni(json);
			
			Finances finances = new ObjectMapper().readValue(obj2, Finances.class);
			financesBusiness.editFinance(finances);
			return this.buildResponse("Finan�a cadastrado com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}
	}
	
	@DELETE
	@Path("del/{id}")
	@Consumes("application/*")
	public Response delete(@PathParam("id") int id){
		try{
			financesBusiness.delete(id);
		return this.buildResponse("Deletado com sucesso");
		}catch(Exception e){
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
			
		}
		
	}
	
	@GET
	@Path("/toearn")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response toEarn(){
		try{
			BigDecimal toearn = financesBusiness.calculateToEarn();
			return this.buildResponse(toearn);
		}catch (Exception e){
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}		
	}
	
	@GET
	@Path("/topay")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response toPay(){
		try{
			BigDecimal topay = financesBusiness.calculateToPay();
			return this.buildResponse(topay);
		}catch (Exception e){
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}		
	}

	@GET
	@Path("/findDateToEarn/{startDate}/{finalDate}")
	@Consumes("application/*")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response findDateToEarn(@PathParam("startDate") String startDate, @PathParam("finalDate") String finalDate){
		try{
			BigDecimal result = financesBusiness.findDateToEarn(startDate, finalDate);
			return this.buildResponse(result);
		}catch (Exception e){
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}		
	}
	
	@GET
	@Path("/findDateToPay/{startDatePay}/{finalDatePay}")
	@Consumes("application/*")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response findDateToPay(@PathParam("startDatePay") String startDate, @PathParam("finalDatePay") String finalDate){
		try{
			BigDecimal result = financesBusiness.findDateToPay(startDate, finalDate);
			return this.buildResponse(result);
		}catch (Exception e){
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}		
	}
	
	@GET
	@Path("/listBetweenDates/{startDatePay}/{finalDatePay}")
	@Consumes("application/*")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})	
	public Response listBetweenDates(@PathParam("startDatePay") String startDate, @PathParam("finalDatePay") String finalDate){
		try{
			List<Finances> result = financesBusiness.listBetweenDates(startDate, finalDate);
			return this.buildResponse(result);
		}catch (Exception e){
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}		
	}
	

	@GET
	@Path("/generateGraphic")
	@Consumes("application/*")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})	
	public Response getToEarn(){
		try{
			List<FinancesVO> result = financesBusiness.generateGraphic();
			return this.buildResponse(result);
		}catch (Exception e){
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}		
	}
	
	
}
