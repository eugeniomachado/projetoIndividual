package br.com.projetoIndividual.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;

import br.com.projetoIndividual.business.SessionBusiness;
import br.com.projetoIndividual.pojo.Session;

@Path("sessionRest")
public class SessionRest extends UtilRest {
	
	public SessionRest() {
	}
	
	SessionBusiness sessbusiness = new SessionBusiness();

	@POST
	@Path("/newSession/")
	@Consumes("application/*")
	public Response newSession (String json) {
		try {
			Session session = new ObjectMapper().readValue(json, Session.class);
			session = sessbusiness.createSession(session);
			
			return this.buildResponse(session);
		} catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}
	}
	
	@GET
	@Path("/listSessions/{id_session}")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response listSessions(@PathParam("id_session") int id_session){
		try{
			List<Session> session = sessbusiness.listSessionsById(id_session);
				
			return this.buildResponse(session);
		}catch (Exception e){
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}		
	}
	
	@GET
	@Path("/searchById/{id_session}")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response searchById(@PathParam("id_session") int id_session){
		try{
			Session session = sessbusiness.find(id_session);
			
			return this.buildResponse(session);
		}catch (Exception e){
			System.out.println("errou");
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}		
	}
	
	@POST
	@Path("/editSession")
	@Consumes("application/*")
	public Response editCustomer (String json) {
		try {
			Session session = new ObjectMapper().readValue(json, Session.class);
			sessbusiness.editSession(session);
			return this.buildResponse("Sess�o editada com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}
	}
	
	@GET
	@Path("/deleteSession/{id_session}")
	@Consumes("application/*")
	public Response deleteSession(@PathParam("id_session") int id_session){
		try{
			sessbusiness.deleteSession(id_session);
			
			return this.buildResponse("Sess�o deletada");
		}catch (Exception e){
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}		
	}
}
