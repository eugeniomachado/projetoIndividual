package br.com.projetoIndividual.hibernateinterface;

import java.util.List;

public interface GenericDAOInterface<T> {
	T save(T entity);
	List<T> getList();
	T find (int id);
	T update(T entity);

}
