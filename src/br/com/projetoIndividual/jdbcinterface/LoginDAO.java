package br.com.projetoIndividual.jdbcinterface;

import java.sql.SQLException;

import br.com.projetoIndividual.objects.Admin;

public interface LoginDAO {
	public boolean validateLogin (Admin admin, String password) throws SQLException;
	
}
