package br.com.projetoIndividual.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.projetoIndividual.hash.Password;
import br.com.projetoIndividual.hash.Password.CannotPerformOperationException;
import br.com.projetoIndividual.hash.Password.InvalidHashException;
import br.com.projetoIndividual.jdbcinterface.LoginDAO;
import br.com.projetoIndividual.objects.Admin;


public class JDBCLoginDAO implements LoginDAO {
	
	private Connection connec;

	public JDBCLoginDAO(Connection connec) {
		this.connec = connec;
	}
	
	public String checkLogin(Admin admin) throws SQLException {
		String sql = "SELECT password FROM admin";
		String password = "";
		
		java.sql.Statement stmt = connec.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		while (rs.next()) {
			password = rs.getString("password");
			validateLogin(admin, password);
		}
		return password;

	}
	
	public boolean validateLogin(Admin admin, String password) throws SQLException {
		try {
			String sql = "SELECT * FROM admin WHERE login=? AND password=?";
			try {
				String senha2 = admin.getPassword();
				System.out.println(password);
				boolean teste = Password.verifyPassword(senha2, password);
				
				if (teste) {
					java.sql.PreparedStatement psmt = connec.prepareStatement(sql);
					psmt.setString(1, admin.getLogin());
					psmt.setString(2, password);
					ResultSet rs = psmt.executeQuery();
					if (rs.next()) {
						return true;
					} else {
						return false;
					}
				}
			} catch (CannotPerformOperationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidHashException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} finally {

		}
		return false;
	}

}

	
