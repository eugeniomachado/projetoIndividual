package br.com.projetoIndividual.business;

import java.util.List;

import br.com.projetoIndividual.hibernate.dao.GenericDAO;
import br.com.projetoIndividual.hibernate.dao.ToDoDAO;
import br.com.projetoIndividual.pojo.ToDo;

public class ToDoBusiness extends GenericDAO<ToDo> {
	
	ToDoDAO dao = new ToDoDAO();
	
	public void create(ToDo toDo) {
		dao.save(toDo);	
	}

	public List<ToDo> list() {
		try {
		List<ToDo> todoList = dao.getList();
		return todoList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void delete(int id) {
		ToDo toDo = dao.find(id);
		dao.remove(toDo);
	}
	
	

}
