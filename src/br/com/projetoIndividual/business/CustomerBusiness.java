package br.com.projetoIndividual.business;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.projetoIndividual.hibernate.dao.CustomerDAO;
import br.com.projetoIndividual.pojo.Customer;
import br.com.projetoIndividual.pojo.CustomerVO;

public class CustomerBusiness {
	public CustomerBusiness() {
	}

	CustomerDAO custdao = new CustomerDAO();

	public void createCustomer(Customer customer) {
		System.out.println(customer);
		custdao.save(customer);
	}

	public Customer find(int id_customer) {
		Customer customer = custdao.find(id_customer);
		return customer;
	}

	public List<Customer> list() {
		List<Customer> list = custdao.getList();
		return list;
	}
	
	public void editCustomer(Customer customer){
		custdao.update(customer);
	}

	public List<Customer> customerStatusGraphic() {
		List<Customer> listStatus = custdao.generateGraphic();
		
		return listStatus;
	}

	public List<Customer> customerServiceGraphic() {
		List<Customer> listServices = custdao.customerServiceGraphic();
		return listServices;
	}

	public List<CustomerVO> getMontlyAnniversary() {
		Calendar cal = Calendar.getInstance();
		String month = new SimpleDateFormat("MM").format(cal.getTime());
		List<CustomerVO> birthdays = custdao.getMontlyAnniversary(month);
		return birthdays;
	}

}
