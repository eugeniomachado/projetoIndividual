package br.com.projetoIndividual.business;

import java.io.BufferedWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.projetoIndividual.hibernate.dao.GenericDAO;
import br.com.projetoIndividual.hibernate.dao.SessionDAO;
import br.com.projetoIndividual.pojo.Customer;
import br.com.projetoIndividual.pojo.Session;

public class SessionBusiness extends GenericDAO<Session> {

	SessionDAO sessdao = new SessionDAO();
	CustomerBusiness custBusiness = new CustomerBusiness();
	
	public Session createSession(Session session){
		try{	

			Date creation = Calendar.getInstance().getTime();
		
		    session.setCreation_time(creation);
			Customer customer = session.getCustomer();		
			int id = customer.getId_customer();
		
			//Format date
			Date date = session.getDate();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String formatDate = df.format(date);
			session.setFormated_date(formatDate);
			
			//find customer
			customer = custBusiness.find(id);
			session.setCustomer(customer);
			sessdao.save(session);
		}catch(Exception e){
			e.printStackTrace();
		}
		return session;
	}
	
	public List<Session> listSessionsById(int id_customer) {
		List<Session> sessions = sessdao.listSessions(id_customer);
		return sessions;
	}
	
	public Session find(int id_session) {
		Session session = sessdao.find(id_session);
		return session;
	}
	
	public void editSession(Session session) {
		try{
			Customer customer  = session.getCustomer();
			int id = customer.getId_customer();
			customer = custBusiness.find(id);
			session.setCustomer(customer);
			sessdao.update(session);	
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	public void deleteSession(int id_session){
		Session session = sessdao.find(id_session);	
		sessdao.remove(session);
		
	}
}
