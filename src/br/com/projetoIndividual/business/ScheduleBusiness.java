package br.com.projetoIndividual.business;

import java.util.List;

import br.com.projetoIndividual.hibernate.dao.ScheduleDAO;
import br.com.projetoIndividual.pojo.Schedule;

public class ScheduleBusiness {

	ScheduleDAO schdao = new ScheduleDAO();

	public void newEvent(Schedule schedule) {
		try {
			schdao.save(schedule);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Schedule> list() {
		try {
			List<Schedule> listEvents = schdao.getList();
			return listEvents;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void editEvent(Schedule schedule) {
		try {
			
			schdao.update(schedule);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void delete(int id) {
		try {
			Schedule schedule = schdao.find(id);
			System.out.println(schedule);
			schdao.remove(schedule);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}