package br.com.projetoIndividual.business;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import br.com.projetoIndividual.hibernate.dao.FinancesDAO;
import br.com.projetoIndividual.pojo.Finances;
import br.com.projetoIndividual.pojo.FinancesVO;

public class FinancesBusiness {

	FinancesDAO financesDAO = new FinancesDAO();

	public void newFinance(Finances finances) {
		dateFormat(finances);
		financesDAO.save(finances);

	}

	public List<Finances> list() {
		try {
			List<Finances> listFinances = financesDAO.list();
			return listFinances;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Finances dateFormat(Finances finances) {
		Date date = finances.getDate();
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String formatDate = df.format(date);
		finances.setFormated_date(formatDate);
	
		return finances;
	}

	public String formatMoni(String obj) throws JSONException {
		try {
			JSONObject jObject = new JSONObject(obj);
			String value = (String) jObject.get("value");
			BigDecimal bigDecimalValue = new BigDecimal(value);
			jObject.put("value", bigDecimalValue);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj;

	}

	public void editFinance(Finances finances) {
		dateFormat(finances);
		financesDAO.update(finances);

	}

	public Finances find(int id_finance) {
		Finances finance = financesDAO.find(id_finance);
		return finance;
	}

	public void delete(int id) {
		try {
			Finances finance = financesDAO.find(id);
			financesDAO.remove(finance);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public BigDecimal calculateToEarn() {
		BigDecimal toEarn = financesDAO.toEarnValues();
		//BigDecimal resultEarn = toEarn.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
		return toEarn;
	}
	
	public BigDecimal calculateToPay() {
		BigDecimal toPay = financesDAO.toPayValues();
		//BigDecimal resultToPay = toPay.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
		return toPay;
	}

	public BigDecimal findDateToEarn(String startDate, String finalDate) {
		List<BigDecimal> valueBetweenDates = financesDAO.findDateToEarn(startDate, finalDate);
		BigDecimal resultToEarnDates = valueBetweenDates.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
		return resultToEarnDates;
	}

	public BigDecimal findDateToPay(String startDatePay, String finalDatePay) {
		List<BigDecimal> valueBetweenDates = financesDAO.findDateToPay(startDatePay, finalDatePay);
		BigDecimal resultToEarnToPay= valueBetweenDates.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
		return resultToEarnToPay;
	}

	public List<Finances> listBetweenDates(String startDate, String finalDate) {
		List<Finances> listFinances = financesDAO.listBetweenDates(startDate, finalDate);
		return listFinances;
	}

	public List<FinancesVO> generateGraphic() {
		List<FinancesVO> listFinances = financesDAO.generateGraphic();
		return listFinances;
	}



}
