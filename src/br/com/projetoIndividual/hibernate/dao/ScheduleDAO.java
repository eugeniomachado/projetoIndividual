package br.com.projetoIndividual.hibernate.dao;

import javax.persistence.EntityManager;

import br.com.projetoIndividual.pojo.Schedule;

public class ScheduleDAO extends GenericDAO<Schedule> {
	
	private EntityManager em = EntityManagerUtil.getEMInstance();
	
	public ScheduleDAO() {
		super(Schedule.class);
	}
	

}
