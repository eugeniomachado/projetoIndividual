package br.com.projetoIndividual.hibernate.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerUtil {
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("projeto");
	private static EntityManager em = null;

	public static EntityManager getEMInstance() {
		if(em == null || !em.isOpen()){
			em = factory.createEntityManager();
		}
		return em;
	}
}
