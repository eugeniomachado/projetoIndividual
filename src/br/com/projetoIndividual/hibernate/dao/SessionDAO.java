package br.com.projetoIndividual.hibernate.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.projetoIndividual.pojo.Session;

public class SessionDAO extends GenericDAO<Session> {
	protected EntityManager entityManager;

	private EntityManager em = EntityManagerUtil.getEMInstance();
	
	public SessionDAO() {
		super(Session.class);
	}
	
	public List<Session>listSessions(int id_customer){
		String hql = "from Session s where fk_id_customer = :id";
		Query query = em.createQuery(hql);
		query.setParameter("id", id_customer);
		@SuppressWarnings("unchecked")
		List<Session> sessions = query.getResultList();
		return sessions;
	
		
	}
	
}
