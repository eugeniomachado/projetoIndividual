package br.com.projetoIndividual.hibernate.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.projetoIndividual.pojo.Customer;
import br.com.projetoIndividual.pojo.CustomerVO;

public class CustomerDAO extends GenericDAO<Customer> {

    private EntityManager em = EntityManagerUtil.getEMInstance();

    public CustomerDAO() {
        super(Customer.class);
    }

    @SuppressWarnings("unchecked")
    public List<Customer> generateGraphic() {
        String hql = "SELECT status, COUNT(*) FROM Customer GROUP BY status;";
        Query query = em.createNativeQuery(hql);
        List<Customer> list = query.getResultList();
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<Customer> customerServiceGraphic() {
        String hql = "SELECT service_type, COUNT(*) FROM Customer GROUP BY service_type";
        Query query = em.createNativeQuery(hql);
        List<Customer> list = query.getResultList();
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<CustomerVO> getMontlyAnniversary(String month) {
        StringBuilder builder = new StringBuilder();
        builder.append(" SELECT c.name, ");
        builder.append(" c.birth_date, ");
        builder.append(" substr(c.birth_date,6,2) ");
        builder.append(" from Customer c ");
        builder.append("where substr(c.birth_date,6,2) ");
        builder.append(" = :month ORDER BY ");
        builder.append(" birth_date DESC ");
        Query query = em.createNativeQuery(builder.toString());
        query.setParameter("month", month);
        @SuppressWarnings("unchecked")
        List<CustomerVO> list = query.getResultList();
        return list;
    }


}
