package br.com.projetoIndividual.hibernate.dao;

import javax.persistence.EntityManager;

import br.com.projetoIndividual.pojo.ToDo;

public class ToDoDAO extends GenericDAO<ToDo> {
	protected EntityManager entityManager;

	private EntityManager em = EntityManagerUtil.getEMInstance();
	
	public ToDoDAO() {
		super(ToDo.class);
	}
	
}
