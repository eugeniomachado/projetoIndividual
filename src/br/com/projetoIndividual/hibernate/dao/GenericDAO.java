package br.com.projetoIndividual.hibernate.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import br.com.projetoIndividual.hibernateinterface.GenericDAOInterface;

public class GenericDAO<T> implements GenericDAOInterface<T> {

	private EntityManager em = EntityManagerUtil.getEMInstance();

	private Class<T> persistedClass;

	public GenericDAO() {
	}

	protected GenericDAO(Class<T> persistedClass) {
		this();
		this.persistedClass = persistedClass;
	}

	public T save(T entity) {
		EntityTransaction t = em.getTransaction();
		try {
			t.begin();
			em.persist(entity);
			t.commit();
			return entity;
		} catch (Exception e) {
			e.printStackTrace();
		return null;
		}
		}

	public List<T> getList() {
			CriteriaBuilder builder = em.getCriteriaBuilder();
			CriteriaQuery<T> query = builder.createQuery(persistedClass);
			query.from(persistedClass);
			return em.createQuery(query).getResultList();
	}

	public T find(int id) {
		return em.find(persistedClass, id);
	}

	public T update(T entity) {
		try {
			EntityTransaction t = em.getTransaction();
			t.begin();
			em.merge(entity);
			t.commit();
			return entity;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void remove(T entity) {
		EntityTransaction tx = em.getTransaction();
		try {
			if (!tx.isActive()) {
				tx.begin();
				em.remove(entity);
				tx.commit();
			}
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}

	}
}