package br.com.projetoIndividual.hibernate.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TemporalType;

import br.com.projetoIndividual.pojo.Finances;
import br.com.projetoIndividual.pojo.FinancesVO;

public class FinancesDAO extends GenericDAO<Finances> {

	public FinancesDAO() {
		super(Finances.class);
	}

	//private EntityManager em = EntityManagerUtil.getEMInstance();

	public BigDecimal toEarnValues() {
		BigDecimal toEarn = new BigDecimal(0);    
		synchronized (EntityManagerUtil.getEMInstance()){
			StringBuilder builder = new StringBuilder();
			builder.append(" SELECT sum(value) ");
			builder.append(" as value from Finances ");
			builder.append(" as f where f.type = 2 ");
			Query query = EntityManagerUtil.getEMInstance().createNativeQuery(builder.toString());
			toEarn = (BigDecimal)query.getSingleResult();
		}
		return toEarn;
	}
	
	public BigDecimal toPayValues() {
		BigDecimal toPay = new BigDecimal(0);
		synchronized (EntityManagerUtil.getEMInstance()){
			StringBuilder builder = new StringBuilder();
			builder.append(" SELECT sum(value) ");
			builder.append(" as value from Finances ");
			builder.append(" as f where f.type = 1 ");
			//String hql = "SELECT sum(value) as value from Finances as f where f.type = 1";
			Query query = EntityManagerUtil.getEMInstance().createNativeQuery(builder.toString());
			toPay = (BigDecimal)query.getSingleResult();
		}
		
		return toPay;
}

	@SuppressWarnings("unchecked")
	public List<BigDecimal> findDateToEarn(String startDate, String finalDate) {
		String hql = "SELECT value from Finances as f where f.type = 2 and f.date BETWEEN '"+ startDate + "' and '" + finalDate + "'";
		Query query = EntityManagerUtil.getEMInstance().createQuery(hql);
		List<BigDecimal> result = query.getResultList();
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<BigDecimal> findDateToPay(String startDatePay, String finalDatePay) {
		String hql = "SELECT value from Finances as f where f.type = 1 and f.date BETWEEN '"+ startDatePay + "' and '" + finalDatePay + "'";
		Query query = EntityManagerUtil.getEMInstance().createQuery(hql);
		List<BigDecimal> result = query.getResultList();
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<Finances> listBetweenDates(String startDatePay, String finalDatePay) {
		String hql = "from Finances as f WHERE f.date BETWEEN '"+ startDatePay + "' and '" + finalDatePay + "'";
		Query query = EntityManagerUtil.getEMInstance().createQuery(hql);
		List<Finances> lista = query.getResultList();
		return lista;
	}

	@SuppressWarnings("unchecked")
	public List<FinancesVO> generateGraphic() {
		List<FinancesVO> lista = new ArrayList<FinancesVO>();
		synchronized (EntityManagerUtil.getEMInstance()){
			StringBuilder builder = new StringBuilder();
			builder.append(" SELECT f.type, ");
			builder.append(" substr(f.formated_date,4,2) ");
			builder.append(" as month, sum(f.value) ");
			builder.append(" as value from Finances f ");
			builder.append(" group by f.type, ");
			builder.append(" f.formated_date ");
			builder.append(" order by f.type, ");
			builder.append(" f.date ");
			//String hql = "SELECT f.type, substr(f.formated_date,4,2) as month, sum(f.value) as value from Finances f group by f.type, f.formated_date order by f.type, f.date";
			Query query = EntityManagerUtil.getEMInstance().createNativeQuery(builder.toString());
			lista = query.getResultList();
		}
		return lista;
		

	}

	@SuppressWarnings("unchecked")
	public List<Finances> list() {
		List<Finances> list = new ArrayList<Finances>();
		synchronized (EntityManagerUtil.getEMInstance()){
			String hql = "from Finances";
			Query query = EntityManagerUtil.getEMInstance().createQuery(hql, Finances.class);
			list = query.getResultList();
		}
		return list;
	}
	
}
