package br.com.projetoIndividual.pojo;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Session implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_session;
	@Temporal(TemporalType.DATE)
	private Date date;
	private boolean present;
	private String session_register;
	private String formated_date;
	private Date creation_time;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name = "fk_id_customer",nullable=false)
	private Customer customer;
	
	public Session() {
		super();
	}
	
	public Session(int id_session, Date date, boolean present, String session_register, String formated_date,
			Customer customer) {
		super();
		this.id_session = id_session;
		this.date = date;
		this.present = present;
		this.session_register = session_register;
		this.formated_date = formated_date;
		this.customer = customer;
	}

	public int getId_session() {
		return id_session;
	}
	public void setId_session(int id_session) {
		this.id_session = id_session;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public boolean isPresent() {
		return present;
	}
	public void setPresent(boolean present) {
		this.present = present;
	}
	public String getSession_register() {
		return session_register;
	}
	public void setSession_register(String session_register) {
		this.session_register = session_register;
	}
	public String getFormated_date() {
		return formated_date;
	}
	public void setFormated_date(String formated_date) {
		this.formated_date = formated_date;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Date getCreation_time() {
		return creation_time;
	}

	public void setCreation_time(Date creation_time) {
		this.creation_time = creation_time;
	}
	
	

	
}
