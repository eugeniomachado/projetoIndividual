package br.com.projetoIndividual.pojo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Customer implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_customer;
	private String name;
	private String sex;
	private Date birth_date;
	private String email;
	private String phone;
	private String indicated_by;
	private String service_type;
	private String level_schooling;
	private String grade_period;
	private String institution;
	private Boolean minor;
	private String father_name;
	private String mother_name;
	private String marital_status;
	private String father_phone;
	private String mother_phone;
	private String status;
	
	public Customer() {
		super();
	}
	
	public Customer(int id_customer, String name, String sex, Date birth_date, String email, String phone,
			String indicated_by, String service_type, String level_schooling, String grade_period, String institution,
			Boolean minor, String father_name, String mother_name, String marital_status, String father_phone, String mother_phone, String status) {
		super();
		this.id_customer = id_customer;
		this.name = name;
		this.sex = sex;
		this.birth_date = birth_date;
		this.email = email;
		this.phone = phone;
		this.indicated_by = indicated_by;
		this.service_type = service_type;
		this.level_schooling = level_schooling;
		this.grade_period = grade_period;
		this.institution = institution;
		this.minor = minor;
		this.father_name = father_name;
		this.mother_name = mother_name;
		this.marital_status = marital_status;
		this.father_phone = father_phone;
		this.mother_phone = mother_phone;
		this.status = status;
	}

	public int getId_customer() {
		return id_customer;
	}

	public void setId_customer(int id_customer) {
		this.id_customer = id_customer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Date getBirth_date() {
		return birth_date;
	}

	public void setBirth_date(Date birth_date) {
		this.birth_date = birth_date;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIndicated_by() {
		return indicated_by;
	}

	public void setIndicated_by(String indicated_by) {
		this.indicated_by = indicated_by;
	}

	public String getService_type() {
		return service_type;
	}

	public void setService_type(String service_type) {
		this.service_type = service_type;
	}

	public String getLevel_schooling() {
		return level_schooling;
	}

	public void setLevel_schooling(String level_schooling) {
		this.level_schooling = level_schooling;
	}

	public String getGrade_period() {
		return grade_period;
	}

	public void setGrade_period(String grade_period) {
		this.grade_period = grade_period;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public Boolean getMinor() {
		return minor;
	}

	public void setMinor(Boolean minor) {
		this.minor = minor;
	}

	public String getFather_name() {
		return father_name;
	}

	public void setFather_name(String father_name) {
		this.father_name = father_name;
	}

	public String getMother_name() {
		return mother_name;
	}

	public void setMother_name(String mother_name) {
		this.mother_name = mother_name;
	}

	public String getMarital_status() {
		return marital_status;
	}

	public void setMarital_status(String marital_status) {
		this.marital_status = marital_status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFather_phone() {
		return father_phone;
	}

	public void setFather_phone(String father_phone) {
		this.father_phone = father_phone;
	}

	public String getMother_phone() {
		return mother_phone;
	}

	public void setMother_phone(String mother_phone) {
		this.mother_phone = mother_phone;
	}

}
