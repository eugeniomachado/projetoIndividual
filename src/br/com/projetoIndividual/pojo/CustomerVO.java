package br.com.projetoIndividual.pojo;

import java.io.Serializable;
import java.util.Date;

public class CustomerVO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String name;
	private Date birth_date;
	private String type;
	
	public CustomerVO(String name, Date birth_date, String type) {
		this.name = name;
		this.birth_date = birth_date;
		this.type = type;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirth_date() {
		return birth_date;
	}
	public void setBirth_date(Date birth_date) {
		this.birth_date = birth_date;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
}
