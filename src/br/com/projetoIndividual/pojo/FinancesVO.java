package br.com.projetoIndividual.pojo;

import java.io.Serializable;
import java.math.BigDecimal;

public class FinancesVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int type;
	private int month;
	private BigDecimal value;
	
	
	public FinancesVO(int type, int month, BigDecimal value){
		this.type = type;
		this.month = month;
		this.value = value;
	}
	
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}

}
