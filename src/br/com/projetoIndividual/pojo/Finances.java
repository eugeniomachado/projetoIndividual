package br.com.projetoIndividual.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Finances implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_finance;
	private String customer_name;
	private Date date;
	private BigDecimal value;
	private int type;
	private String formated_date;
	private String comment;
	
	public Finances() {
		super();
	}

	public int getId_finance() {
		return id_finance;
	}

	public void setId_finance(int id_finance) {
		this.id_finance = id_finance;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}


	public String getFormated_date() {
		return formated_date;
	}


	public void setFormated_date(String formated_date) {
		this.formated_date = formated_date;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}



	
	
}
