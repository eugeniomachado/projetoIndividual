var url = "/projetoIndividual/rest/toDoRest/";
Dashboard = new Object();

$(document).ready( function() {

	var inProgress = 0;

    function handleBefore() {
        inProgress++;
    };

    function handleComplete() {
        if (!--inProgress) {
            // do what's in here when all requests have completed.
        }
    };

	  Dashboard.list = function(list) {
      	  var arr = [];

      	  if (list != undefined && list.length > 0 && list[0].id_todo != undefined) {

      	      for (i = 0; i < list.length; i++) {
      	          $('ol').append('<li data-id="' + list[i].id_todo + '">' + list[i].comment + '</li>');
      	      }

      	  } else {
      	      if (list == undefined || (list != undefined && list.length > 0)) {
      	          $.ajax({
      	              url: url + 'list',
      	              type: "GET",
      	              contentType: "application/json; charset=utf-8",
      	              dataType: 'json',
      	              beforeSend: handleBefore,
      	              success: function(list) {
      	            	  Dashboard.list(list);
      	            	  handleComplete();
      	              },
      	              error: function(err) {
      	                  $.notify({
      	                      message: 'Problemas ao recuperar os lembretes!'
      	                  }, {
      	                      type: 'danger'
      	                  });
      	              }
      	          })
      	      } else {
      	          $('ol').append('<li> tem nada aqui </li>');
      	      }
      	  }
      	  }
	  		Dashboard.list(undefined, "")


            $('#butao').click(function() {
                    var toAdd = $('input[name=ListItem]').val();
                    var json = {
                        comment: toAdd
                    }

                    $.ajax({
                        url: url + 'new',
                        type: "POST",
                        data: JSON.stringify(json),
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        beforeSend: handleBefore,
                        success: function(data) {
                            $("#content").load("/projetoIndividual/resources/dashboard/dashboard.html");
                            handleComplete();
                        },
                        error: function(err) {
                            $.notify({
                                message: 'Problemas ao cadastrar um novo registro financeiro!'
                            }, {
                                type: 'danger'
                            });
                        }
                    })



                $("input[name=ListItem]").keyup(function(event) {
                    if (event.keyCode == 13) {
                        $("#butao").click();
                    }
                });


                 $(document).on('dblclick', 'li', function(e) {
                        var id = $(this).attr('data-id');
                        $(this).toggleClass('strike').fadeOut('slow');
                        setTimeout(function(){
                        $.ajax({
                            type: "DELETE",
                            url: url + "del/" + id,
                            dataType: "json",
                            success: function() {
                            	 handleComplete();
                            },
                            error: function(msg) {


                            }
                        }, 500);
                       });


                $('input').focus(function() {
                    $(this).val('');
                });

                $('ol').sortable();
            });
      	  });
            $(document).ready(function() {

                var arrDates = [];
                var arrNames = [];
                setTimeout(function(){
                $.ajax({
                    type: "GET",
                    url: "/projetoIndividual/rest/customerRest/getMontlyAnniversary",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) {
                        for (var i = 0; i < data.length; i++) {
                            arrNames.push(data[i][0]);
                        }

                        for (var i = 0; i < data.length; i++) {
                            arrDates.push(data[i][1]);
                        }

                        showBirthday(arrNames, arrDates);
                    },
                    error: function(msg) {}
                }, 500);
                });
            })


            showBirthday = function(arrNames, arrDates) {

                var html = "<table class='table table-responsive' id='session-table'>";
                html += "<tr><th>Nome</th><th>Data</th></tr>";
                if (arrNames != undefined && arrNames.length > 0) {
                    for (var i = 0; i < arrNames.length; i++) {
                        html += "<tr>" +
                            "<td>" + arrNames[i] + "</td>" +
                            "<td>" + dataConverter(arrDates[i]) + "</td>" +
                            "</td>" +
                            "</tr>";
                    }

                } else {
                    html += "<tr><td colspan='3'>Nenhum aniversariante no mês</td></tr>";
                }
                html += "</table>";
                $("#resultBirthday").html(html);
            };

            dataConverter = function(arrDates) {
                var date = new Date(arrDates*1000);
                var day = date.getDay();
                var month =  date.getMonth();
                var year = date.getYear();
                var formattedTime = day + '-' + month + '-' + year;
                console.log(formattedTime);
                if (formattedTime.indexOf("-") > 0) {
                    dataBR = data.split("-");
                    return dataBR[1] + "/" + dataBR[2] + "/" + dataBR[0];

                }
            }
            financesGraphics = function(arrFinal) {
                var ctx = document.getElementById('myChart').getContext('2d');
                var varLabels = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
                var varPagar = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                var varReceber = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

                if (typeof arrFinal !== 'undefined') {
                    arrFinal.forEach(function(reg) {
                        if (typeof reg !== 'undefined' && reg.length > 0) {
                            if (reg[0] == 1) {
                                varPagar[reg[1] - 1] += reg[2];
                            } else {
                                varReceber[reg[1] - 1] += reg[2];
                            }
                        }
                    });
                }
                var chart = new Chart(ctx, {

                    // The type of chart we want to create
                    type: 'line',

                    // The data for our dataset
                    data: {
                        labels: varLabels,
                        datasets: [{
                                label: "A receber",
                                backgroundColor: 'rgb(117, 209, 0)',
                                borderColor: 'rgb(117, 209, 0)',
                                data: varReceber,
                                fill: false,
                            },

                            {
                                label: "A pagar",
                                backgroundColor: 'rgb(229, 15, 0)',
                                borderColor: 'rgb(229, 15, 0)',
                                data: varPagar,
                                fill: false,
                            }
                        ]

                    },
                    // Configuration options go here
                    options: {}

                })
            }

            $('#agenda').fullCalendar({
                lang: 'pt-br',
                defaultView: 'agendaDay',
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayView'
                },
                minTime: "06:00:00",
                maxTime: "18:00:00",
                buttonText: {
                    today: 'Hoje',
                    month: 'Mês',
                    week: 'Semana',
                    day: 'Dia',
                    list: 'Lista'

                },


                editable: true,

                eventDrop: function(event, delta, revertFunc, jsEvent, ui, view, start, end) {
                    Dashboard.update(event.start.format(),event.id, event.end.format(), event.title);

                },
                eventClick: function(event) {
                	Dashboard.info(event);

                }

            });

            Dashboard.showSchedule = function(eventList) {
                if (eventList != undefined && eventList.length > 0 && eventList[0].id != undefined) {
                    for (var i = 0; i < eventList.length; i++) {
                        var tooltip = "";
                        tooltip = eventList[i].name.split(" ");
                        tooltip = tooltip[0];

                        var teste = {
                            tudo: eventList[i],
                            id: eventList[i].id,
                            title: tooltip,
                            start: eventList[i].date_time,
                            end: eventList[i].end_time,
                            borderColor: "#333399",
                            tooltip: eventList[i].name,
                            color: eventList[i].color
                        }

                        $('#agenda').fullCalendar('renderEvent', teste, true);
                    }
                } else {
                    if (eventList == undefined || (eventList != undefined && eventList.length > 0)) {
                        $.ajax({
                            type: "GET",
                            url: '/projetoIndividual/rest/scheduleRest/listEvents',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function(eventList) {
                            	Dashboard.showSchedule(eventList);
                            },
                            error: function(err) {
                                $.notify({
                                    message: 'Houve um erro ao carregar os agendamentos'
                                }, {
                                    type: 'danger'
                                });
                            }
                        });
                    } else {
                        $.notify({
                            message: 'Não há agendamentos'
                        }, {
                            type: 'warning'
                        });
                    }

                }
            },
            $(document).ready(Dashboard.showSchedule());
        });