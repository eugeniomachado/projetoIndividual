Scheduler = new Object();

$(".datepicker").datepicker({
    language: 'pt-BR',
    format: 'dd/mm/yyyy'
});

$.notifyDefaults({
    placement: {
        from: "bottom",
        align: "center"
    },
});

$(document).ready(function() {
	 $('.time').mask('00:00');
	  
    $('#scheduleDetails').on('show.bs.modal', function(event) {

    })

    var date = new Date();
    var d = date.getDate(),
        m = date.getMonth(),
        y = date.getFullYear();


    $('#calendar').fullCalendar({
        lang: 'pt-br',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'agendaWeek,agendaDay,month,listMonth'
        },
        minTime: "06:00:00",
        maxTime: "22:00:00",
        buttonText: {
            today: 'Hoje',
            month: 'Mês',
            week: 'Semana',
            day: 'Dia',
            list: 'Lista'

        },


        editable: true,

        eventDrop: function(event, delta, revertFunc, jsEvent, ui, view, start, end) {
        	console.log(event);
            Scheduler.update(event.start.format(),event.id, event.end.format(), event.title);

        },
        eventClick: function(event) {
            Scheduler.info(event);

        }
        

    });

    Scheduler.showSchedule = function(eventList) {
            if (eventList != undefined && eventList.length > 0 && eventList[0].id != undefined) {
                for (var i = 0; i < eventList.length; i++) {
                    var tooltip = "";
                    tooltip = eventList[i].name.split(" ");
                    tooltip = tooltip[0];

                    var teste = {
                        tudo: eventList[i],
                        id: eventList[i].id,
                        title: tooltip,
                        start: eventList[i].date_time,
                        end: eventList[i].end_time,
                        borderColor: "#333399",
                        tooltip: eventList[i].name,
                        color: eventList[i].color
                    }

                    $('#calendar').fullCalendar('renderEvent', teste, true);
                }
            } else {
                if (eventList == undefined || (eventList != undefined && eventList.length > 0)) {
                    $.ajax({
                        type: "GET",
                        url: '/projetoIndividual/rest/scheduleRest/listEvents',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(eventList) {
                            Scheduler.showSchedule(eventList);
                        },
                        error: function(err) {
                            $.notify({
                                message: 'Houve um erro ao carregar os agendamentos'
                            }, {
                                type: 'danger'
                            });
                        }
                    });
                } else {
                    $.notify({
                        message: 'Não há agendamentos'
                    }, {
                        type: 'warning'
                    });
                }

            }
        },
        $(document).ready(Scheduler.showSchedule());

    Scheduler.info = function(event) {
    	console.log(event); 
    	var startformat = event.tudo.date_time;
    	var startHour = startformat.slice(11);
    	var startDate = startformat.slice(-20, 10);
   
    
    	var date = startDate.split("-");
        date = date[2] + "/" + date[1] + "/" + date[0];

    	var endformat = event.end._i;
    	var hourEnd= endformat.slice(11);
    	var dateEnd = endformat.slice(-20, 10);
    	
        var dateend = dateEnd.split("-");
        dateend = dateend[2] + "/" + dateend[1] + "/" + dateend[0];
    
        alertify.myAlert(
            "Nome: " + event.tudo.name + "<br>" +
            "<br>Data Inicial: " + date  +
            " Horario inicial: " + startHour +
            "<br>Data Final: " + dateend + " Horario Final: " + hourEnd, 
            event

        ).set('height',false).resizeTo('100%')
        		
    }



    Scheduler.save = function() {
            var obj = new Object();
            var error = false;
            debugger
            var name = $("#fullName").val();
            var startDateValidator =  $("#consultDate").val();
            var endDateValidator = $("#consultDate2").val();
            var starTime = $("#schedule").val();
            var endTime = $("#consultDate2").val();
            
            if (name == ""){
            	$("#namediv").addClass("has-error");
            	$("#fullName").focus();
            	error = true;
            }
            
            if (startDateValidator == ""){
            	$("#datestart").addClass("has-error");
            	$("#consultDate").focus();
            	error = true;
            }
            
            if (endDateValidator == ""){
              	$("#endDate").addClass("has-error");
            	$("#consultDate2").focus();
            	error = true;
            }
            
            if (starTime == ""){
              	$("#timestart").addClass("has-error");
              	$("#schedule").focus();
            	error = true;
            }
            
            if(endTime == ""){
            	$("#endTime").addClass("has-error");
            	$("#consultDate2").focus();
            	error = true;
            }
          
            if (error){
                $.notify({
                    message: 'Favor Preencher os campos obrigatórios'
                }, {
                    type: 'danger'
                });
            	return false;
            }
            
            var date = $("#consultDate").val().split("/");
            date = date[2] + "-" + date[1] + "-" + date[0];
            
            var date2 = $("#consultDate2").val().split("/");
            date2 = date2[2] + "-" + date2[1] + "-" + date2[0];

            obj.name = $("#fullName").val();
            obj.date_time = date + " " + $("#schedule").val() + ":00";
            obj.end_time = date2 + " " + $("#schedulehour2").val() + ":00";
            obj.color = $("#sel option:selected").val();
            

            $.ajax({
                type: "POST",
                url: '/projetoIndividual/rest/scheduleRest/newEvent',
                data: JSON.stringify(obj),
                success: function(obj) {
                    Scheduler.showSchedule();
                    $('#calendar').fullCalendar('removeEventSources');
                    $.notify({
                        message: 'Agendamento efetuado com sucesso!'
                    }, {
                        type: 'success'
                    });
                },
                error: function(msg) {
                    $.notify({
                        message: 'Houve um erro ao tentar salvar o agendamento' + msg
                    }, {
                        type: 'danger'
                    });

                }
            });
        },

        Scheduler.deleteSchedule = function(id) {
            var data = {};
            data.id = id;
            $.ajax({
                type: "DELETE",
                url: '/projetoIndividual/rest/scheduleRest/delete/' + id,
                data: JSON.stringify(data),
                success: function(data) {
                    $.notify({
                        message: 'Agendamento removido com sucesso!'
                    }, {
                        type: 'success'
                    });
                    $("#calendar").fullCalendar('removeEvents', id);
                },
                error: function(msg) {
                    console.log(msg)
                    alertify.error(msg.responseText);

                }
            });
            return true;
        }
});

Scheduler.update = function(new_date,id, end, name, title){
	var data = {};
	new_date = new_date.split("T");
	new_date = new_date[0]+" "+new_date[1];
	
	end = end.split("T");
	end = end[0] + " " + end[1];
	
	data.id = id; 
	data.name = name;
	data.date_time = new_date;
	data.end_time = end;
	
	console.log(data);
		$.ajax({
			type: "PUT",
			url: "/projetoIndividual/rest/scheduleRest/update",
	        data: JSON.stringify(data),
			success:function(data){
                $.notify({
                    message: 'Agendamento alterado com sucesso!'
                }, {
                    type: 'success'
                });
			},
			error:function(msg){
				  $.notify({
	                    message: 'Houve um erro ao alterar o agendamento!'
	                }, {
	                    type: 'success'
	                });
			}
		});
} 


/*AlertFY*/

if (!alertify.myAlert) {
    alertify.dialog('myAlert', function factory() {
        return {
            main: function(message, event) {
                this.message = message;
                this.event = event;
            },
            setup: function() {
                return {
                    buttons: [
                        {
                            text: "Fechar",
                            key: 27,
                            className: alertify.defaults.theme.ok
                        },
                        {
                            text: "Excluir",
                            className: alertify.defaults.theme.cancel
                        }  
                    ],
                    focus: {
                        element: 0
                    },
                    options: {
                        title: "Informações do agendamento"
                    },

                };
            },
            prepare: function() {
                this.setContent(this.message);
            },
            callback: function(closeEvent) {
                if (closeEvent.index == 1) {
                    Scheduler.deleteSchedule(this.event.id);
                }
            }
            
        }
        alertify.alert('Resized to 100%,250px').set('resizable',true).resizeTo('100%',250); 
    });

}