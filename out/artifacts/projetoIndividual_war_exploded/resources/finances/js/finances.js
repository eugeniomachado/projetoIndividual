Finances = new Object();
var url = "/projetoIndividual/rest/financesRest/";
var arrayObjects = [];

$.notifyDefaults({
    placement: {
        from: "top",
        align: "center"
    },
});

var arrayDates = [];

$(document).ready(function() {
        $('.money').mask("###0.00", {
            reverse: true
        });
        Finances.list();
        $('#toEarnModal').on('show.bs.modal', function(event) {})

        $('#financeEdit').on('show.bs.modal', function(event) {})
        
        $('#confirmDeleteModal').on('show.bs.modal', function(event) {}) 	
        
        
        $.ajax({
	        type: "GET",
	        url: url + "toearn",
	        dataType: "json",
	        success: function(data) {
	        	var value = accounting.formatMoney(data, "R$ ");
	            $("#tagToEarn #toEarn").text(value);
	        	
	        },
	        error: function(msg) {
	        }
    });
        

        Finances.save = function() {
            var sVal = $('#financeValue').val();
            //  fValue = Number(sVal.replace(/[^\d]+/g,''));
            var json = {
                customer_name: $('#customerName').val(),
                date: $('#toPayDate').val(),
                value: sVal,
                type: $('#selectFinances').find(":selected").val(),
                comment: $('#comment').val(),

            }
	            $.ajax({
	                url: url + "new",
	                type: "POST",
	                data: JSON.stringify(json),
	                contentType: "application/json; charset=utf-8",
	                dataType: 'json',
	                success: function(data) {
	                    $('#newCustomer').modal('hide');	              
	                    $.notify({
	                        message: 'Registro financeiro cadastrado com sucesso!'
	                    }, {
	                        type: 'success'
	                    });
	                    Finances.list();
	                    
	                },
	                error: function(err) {
	                    $.notify({
	                        message: 'Problemas ao cadastrar um novo registro financeiro!'
	                    }, {
	                        type: 'danger'
	                    });
	                }
	            })	
        }
       
       
    },

    refresher = function(){
        $("#content").load("/projetoIndividual/resources/finances/finances.html");
    },
    
    Finances.list = function(listFinance) {
    	var arrProfit = [];
    	var arrToPay = [];
        if(listFinance != undefined && listFinance.length > 0 && listFinance &&
        		listFinance[0].id_finance != undefined){
	        listFinance.forEach(function(obj) {
	            var statuscheck = obj.status;
	            var teste = obj.value;
	            var final = accounting.formatMoney(teste, "R$ ");
	            var linkedit = '<a data-toggle="modal" href="#financeEdit" class="editidfinan" data-id="' + obj.id_finance + '" ><i class="pe-7s-plus"></a>';
	            var linkdel = '<a data-toggle="modal" href="#confirmDeleteModal" class="deldfinan" data-id="' + obj.id_finance + '" ><i class="pe-7s-close"></i></a>';
	            if (obj.type == 1) {
	                arrToPay.push([obj.customer_name, obj.formated_date, final, linkedit, linkdel]);
	            } else {
	                arrProfit.push([obj.customer_name, obj.formated_date, final, linkedit, linkdel]);
	            }
	        })
	         

        $('#toPay').DataTable({
            data: arrToPay,
            bDestroy: true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
            }
        })

        $('#toEarn').DataTable({
            data: arrProfit,
            bDestroy: true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
            }
        })
        }else{
	            $.ajax({
	            	type : "GET",
	            	url : url + "list",
	            	contentType : "application/json; charset=utf-8",
	            	dataType : "json",
	            	success : function(listFinance) {
	            		Finances.list(listFinance);
	
	            	},
	            	error : function(err) {
	
	            	}
	            });	 
        	
        }
       

    })
    
Finances.findById = function(id_finance) {
    $.ajax({
        type: "GET",
        url: url + "find/" + id_finance,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(finance) {
        	var date = new Date(finance.date);
        	var financedate = date.toISOString().slice(0, 10);
        	$('#id_finance').val(finance.id_finance);
            $('#selectFinancesEdit').val(finance.type);
            $('#customerNameEdit').val(finance.customer_name);
            $('#toPayDateEdit').val(financedate);
            $('#financeValueEdit').val(finance.value);
            $('#commentEdit').val(finance.comment);
            
             
        },
        error: function(err) {
            $.notify({
                message: 'Erro ao localizar as informações do registro'
            }, {
                type: 'danger'
            });
        }
    })
},

Finances.edit = function() {
	   var sVal = $('#financeValueEdit').val();
	   var json = {
			   id_finance: $('#id_finance').val(),
               customer_name: $('#customerNameEdit').val(),
               date: $('#toPayDateEdit').val(),
               value: sVal,
               type: $('#selectFinancesEdit').find(":selected").val(),
               comment: $('#commentEdit').val(),

           }
	   $.ajax({
	        type: "POST",
	        url: url + "edit",
	        contentType: "application/json; charset=utf-8",
	        data: JSON.stringify(json),
	        dataType: "json",
	        success: function() {
	        	$('#financeEdit').modal('hide');
	            $("#content").load("/projetoIndividual/resources/finances/finances.html");

		        $.notify({
		            message: 'Registro financeiro editada com sucesso'
		        }, {
		            type: 'success'
		        });
	        },
	        error: function(err) {
	            $.notify({
	                message: 'Erro ao cadastrar uma nova sessão'
	            }, {
	                type: 'danger'
	            });
	        }
	    })	
	  
	
},

Finances.del = function(id_finance) {
    var id = $('#id_finance').val();
    $.ajax({
        type: "DELETE",
        url: url + "del/" + id,
        dataType: "json",
        success: function() {
        	   $.notify({
		            message: 'Deletado com sucesso'
		        }, {
		            type: 'success'
		        });
        	   refresher();
        },
        error: function(msg) {


        }
    });
},

Finances.listBetweenDates = function(){

	var startDate = $('#startDate').val();
	var fDate = $('#finalDate').val();

	if(startDate && fDate !== null || startDate && fDate !== undefined || startDate && fDate !== ""){
		$.ajax({
			type: "GET",
			url: url + "listBetweenDates/" + startDate + "/"+ fDate,
			contentType: "application/json; charset=utf-8",
			dataType: 'json',
			success: function(financesList) {
				Finances.list(financesList);
			},
			error: function(msg) {

			}
		});
	}
	
}


Finances.searchToEarn = function(){
	var startDate = $('#startDate').val();
	var fDate = $('#finalDate').val();

	if(startDate && fDate !== null || startDate && fDate !== undefined || startDate && fDate !== ""){
		$.ajax({
			type: "GET",
			url: url + "findDateToEarn/" + startDate + "/"+ fDate,
			contentType: "application/json; charset=utf-8",
			dataType: 'json',
			success: function(data) {
		    	var valueNew = accounting.formatMoney(data, "R$ ");
	            $("#tagToEarn #toEarn").text(valueNew);
			},
			error: function(msg) {

			}
		});
	}
},

Finances.searchToPay = function(){
	var startDate = $('#startDate').val();
	var fDate = $('#finalDate').val();
	
	if(startDate && fDate !== null || startDate && fDate !== undefined || startDate && fDate !== ""){
		$.ajax({
			type: "GET",
			url: url + "findDateToPay/" + startDate + "/"+ fDate,
			contentType: "application/json; charset=utf-8",
			dataType: 'json',
			success: function(data) {
		    	var valueNew = accounting.formatMoney(data, "R$ ");
	            $("#tagToPay #toPay").text(valueNew);
			},
			error: function(msg) {

			}
		});
	}
},

$(document).on('click', '.editidfinan', function() {
    var id_finance = $(this).attr('data-id');
    $("#id_finance").attr("value", id_finance);
    Finances.findById(id_finance);
});

$(document).on('click', '.deldfinan', function() {
    var id_finance = $(this).attr('data-id');
    $("#id_finance").attr("value", id_finance);
});

Finances.clearToEarnTotal = function(){
	refresher();
}



$(document).ready(function() {

	 
setTimeout(function(){
	 $.ajax({
	        type: "GET",
	        url: url + "topay",
	        dataType: "json",
	        success: function(data) {
	        	var value = accounting.formatMoney(data, "R$ ");
	
	            $("#tagToEarn #toEarn").text(value);
	        	
	        },
	        error: function(msg) {
	        }
	    });
}, 500);

 setTimeout(function(){
		$.ajax({
	        type: "GET",
	        url: url + "toearn",
	        contentType: "application/json; charset=utf-8",
	        dataType: "json",
	        success: function(data) {
	           	var value = accounting.formatMoney(data, "R$ ");
	            $("#tagToPay #toPay").text(value);
	        }
		
	     });    
     	
 }, 500);

});

$(document).ready(function() {


	 
var arr = [];
var arrFinal = [];

$.ajax({
    type: "GET",
    url: url + "generateGraphic",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function(data) {

    	
    	  /* data.forEach(function (obj){
        		arrayTest.push([obj.formated_date, obj.value])
        	});
        	*/
    	if (data !== null){
    		Finances.graphics(data);
    	}
    },
    error: function(msg) {
    }
});  


})



Finances.graphics = function(arrFinal){	
	var ctx = document.getElementById('myChart').getContext('2d');
	
	
	var varLabels = ['Janeiro', 'Fevereiro', 'Março', 'Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'];
	var varPagar = [0,0,0,0,0,0,0,0,0,0,0,0];
	var varReceber = [0,0,0,0,0,0,0,0,0,0,0,0];
	
	if(typeof arrFinal !== 'undefined'){
		arrFinal.forEach(function(reg){
			if(typeof reg !== 'undefined' && reg.length > 0){
				if(reg[0] == 1){
					varPagar[reg[1] - 1] += reg[2];
				} else {
					varReceber[reg[1] - 1] += reg[2];
				}
			}
		});
	}
	var chart = new Chart(ctx, {
	    type: 'line',
	    data: {
	        labels: varLabels,
	        datasets: [{
	            label: "A receber",
	            backgroundColor: 'rgb(117, 209, 0)',
	            borderColor: 'rgb(117, 209, 0)',
	            data: varReceber,  
	            fill: false,
	        },
	        
	        {
	            label: "A pagar",
	            backgroundColor: 'rgb(229, 15, 0)',
	            borderColor: 'rgb(229, 15, 0)',
	            data: varPagar,
	            fill: false,
	        }]
	
	    },

	    options: {}

	})
	
}

